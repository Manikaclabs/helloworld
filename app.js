process.env.NODE_ENV = 'localDevelopment';
config = require('config');

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();
var routes = require('./routes/index');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.get('/', routes);

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});